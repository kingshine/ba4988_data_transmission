/******************************************************************************************
*				
*	Copyright (c)2002 , 广东步步高教育电子分公司
*	All rights reserved.
**
*	文件名称：keytable.h
*	文件标识：BA818系统
*	摘    要：BA818键盘扫描码表和转换后的键盘消息值
**
*	修改历史：
*	版本    	日期    	 作者    	 改动内容和原因
*   ------		-------		---------	------------------------------
*	1.0    		2002.08.07   任新村      完成基本内容
*******************************************************************************************/
#ifndef		KEYTABLE_H
#define		KEYTABLE_H

/*******************************************************************************************
*				转换后的扩展键  ---  功能键
*		说明：功能键对应消息类型WM_CHAR_FUN,取消息值(param)的低字节
*
******************************************* ************************************************/
#define		CHAR_LW_DICT			0x01
#define		CHAR_CE_DICT			0x02
#define		CHAR_YS_DICT			0x03
#define		CHAR_INSERT				0x04
#define		CHAR_DEL				0x05
#define		CHAR_SEARCH				0x06
#define		CHAR_MODIFY				0x07
#define		CHAR_MEMO_ADDRESS		0x08

#define		CHAR_CALC				0x09
#define		CHAR_ALARM				0x0a
#define		CHAR_COMM_GAME			0x0b
#define		CHAR_TIME_SET			0x0c	
#define		CHAR_EXAM_OTHER			0x0d
#define		CHAR_SPK				0x0e
#define		CHAR_REPEATER			0x0f
#define		CHAR_ON_OFF				0x10

#define		CHAR_F1					0x11
#define		CHAR_F2					0x12
#define		CHAR_F3					0x13
#define		CHAR_F4					0x14
#define		CHAR_F5					0x15
#define		CHAR_F6					0x16
#define		CHAR_F7					0x17
#define		CHAR_F8					0x18

#define		CHAR_PGUP				0x20
#define		CHAR_PGDN				0x21
#define		CHAR_UP					0x22
#define		CHAR_DOWN				0x23
#define		CHAR_LEFT				0x24
#define		CHAR_RIGHT				0x25
#define		CHAR_HELP				0x26
#define		CHAR_ENTER				0x27
#define		CHAR_EXIT				0x28
#define		CHAR_ZYS				0x29			/*中英数*/
#define		CHAR_INPUT				0x2a
#define		CHAR_SYM				0x2b


/*******************************************************************************************
*				转换后的扩展键  ---  主要是数学运算符号
*		说明：数学运算符号对应消息类型WM_CHAR_MATH,取消息值(param)的低字节
*
*******************************************************************************************/
#define		CHAR_SIN		0x01
#define		CHAR_COS		0x02
#define		CHAR_TAN		0x03
#define		CHAR_1_X		0x04
#define		CHAR_DIV		0x05
#define		CHAR_PER		0x06			/*	%	*/
#define		CHAR_M_ADD		0x07
#define		CHAR_LOG		0x08
#define		CHAR_LN			0x09
#define		CHAR_XY			0x0a
#define		CHAR_SQRT		0x0b
#define		CHAR_MUL		0x0c
#define		CHAR_SIGN		0x0d		
#define		CHAR_BKT		0x0e			/*	()	*/
#define		CHAR_PI			0x0f
#define		CHAR_EXP		0x10
#define		CHAR_C			0x11
#define		CHAR_SUB		0x12
#define		CHAR_MC			0x13
#define		CHAR_AC			0x14
#define		CHAR_EQ			0x15
#define		CHAR_DOT		0x16
#define		CHAR_ADD		0x17
#define		CHAR_MR			0x18
#define		CHAR_M_SUB		0x19

#define		CHAR_0			0x30
#define		CHAR_1			0x31
#define		CHAR_2			0x32
#define		CHAR_3			0x33
#define		CHAR_4			0x34
#define		CHAR_5			0x35
#define		CHAR_6			0x36
#define		CHAR_7			0x37
#define		CHAR_8			0x38
#define		CHAR_9			0x39


/*******************************************************************************************
*							键盘扫描码
*		说明：对应消息类型WM_KEY,取消息值(param)的低字节
*
*******************************************************************************************/

#define		KEY_F1					0x34	
#define		KEY_F2					0x35
#define		KEY_F3					0x36
#define		KEY_F4					0x37
#define		KEY_F5					0x3c
#define		KEY_F6					0x3d
#define		KEY_F7					0x3e
#define		KEY_F8					0x3f

#define		KEY_ON_OFF				0x00
#define		KEY_LW_DICT				0x01
#define		KEY_CE_DICT				0x02
#define		KEY_YS_DICT				0x03
#define		KEY_INSERT				0x04
#define		KEY_DEL					0x05
#define		KEY_SEARCH				0x06
#define		KEY_MODIFY				0x07

#define		KEY_MEMO_ADDRESS		0x08
#define		KEY_CALC				0x09
#define		KEY_ALARM				0x0a
#define		KEY_COMM_GAME			0x0b
#define		KEY_TEME_SET			0x0c	
#define		KEY_EXAM_OTHER			0x0d
#define		KEY_SPK					0x0e
#define		KEY_REPEATER			0x0f

#define		KEY_Q_SIN				0x10
#define		KEY_W_COS				0x11
#define		KEY_E_TAN				0x12
#define		KEY_R_1X				0x13
#define		KEY_T_7					0x14
#define		KEY_Y_8					0x15
#define		KEY_U_9					0x16
#define		KEY_I_DIV				0x17
#define		KEY_O_PERCENT			0x30
#define		KEY_P_M_ADD				0x38

#define		KEY_A_LOG				0x18
#define		KEY_S_LN				0x19
#define		KEY_D_XY				0x1a
#define		KEY_F_SQRT				0x1b
#define		KEY_G_4					0x1c
#define		KEY_H_5					0x1d
#define		KEY_J_6					0x1e
#define		KEY_K_MUL				0x1f
#define		KEY_L_SIGN				0x31			/*	L/+/-		*/
#define		KEY_PGUP_M_SUB			0x39

#define		KEY_Z_BRACKET			0x20
#define		KEY_X_PI				0x21
#define		KEY_C_EXP				0x22
#define		KEY_V_C					0x23
#define		KEY_B_1					0x24
#define		KEY_N_2					0x25
#define		KEY_M_3					0x26
#define		KEY_UP_SUB				0x27	
#define		KEY_HELP				0x32
#define		KEY_PGDN_MC				0x3a

#define		KEY_EXIT_AC				0x28
#define		KEY_ZYS_SHIFT			0x29
#define		KEY_INPUT				0x2a
#define		KEY_SPACE_EQ			0x2b
#define		KEY_SYM_0				0x2c
#define		KEY_DOT					0x2d
#define		KEY_LEFT				0x2e
#define		KEY_DOWN_ADD			0x2f
#define		KEY_RIGHT				0x33
#define		KEY_ENTER_MR			0x3b


#endif			/*  KEYTABLE_H  */
