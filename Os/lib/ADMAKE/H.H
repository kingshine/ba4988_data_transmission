/*************************************************************************
|                                                                        |
|   H.H                                                         24.08.94 |
|   ADMAKE Utility:  header file                                         |
|                                                                        |
*************************************************************************/

#define uchar       unsigned char
#define bool        uchar
#define TRUE        (1)
#define FALSE       (0)
#define DEFN1       "makefile"              /*  Default names  */
#define DEFN2       "Makefile"              /*  Default names  */
#define LIFNAME     "TEMP_LIF.TMP"
#define LZ          (4096)                  /*  Line size  */

#define DOLLAR   1          /* special dollar processing */
#define PANEWFD  1          /* dft new-style function declarations */
#define PFPROTO  1          /* dft new-style function prototypes */
#define VOID     void       /* undefine for old compilers */
#define MAIN     int	    /* undefine for old compilers */

extern char * MSG[];        /* error message texts */

#ifdef SWAPEXEC
#include "exec.h"
#endif

#ifdef VMS
#undef MAIN
#define MAIN
#define CDECL
#define PXNORMAL 1          /* normal exit() arg */
#define PXERROR  0          /* error exit() arg */
#define unlink(filename) remove(filename)
#define STRDUP_NEEDED       /* need local strdup, not in compiler library */
#else
#define PXNORMAL 0          /* normal exit() arg */
#define PXERROR  1          /* error exit() arg */
#endif

#ifdef NIX
#undef PANEWFD              /* Old function declarations */
#undef PFPROTO              /* Old function prototypes */
#define VARARGS  1          /* Use Varargs */
#define CDECL               /* No cdecl */
#ifdef COHERENT
#define STRDUP_NEEDED       /* need local strdup, not in compiler library */
#define VFPRINTF_MISSING    /* no vfprintf, work around it */
#endif
#endif

#ifdef __BORLANDC__         /* select PAMDOS or PAMOS2 on compile command */
#define CDECL
#define SPLITPATH
#endif

#ifdef _MSC_VER             /* select PAMDOS or PAMOS2 on compile command */
#define CDECL _cdecl
#define SPLITPATH
#if _MSC_VER >= 600
#define MSC6
#endif
#endif

#ifdef _INTELC32_
#define CDECL
#define SPLITPATH
#define PAMDOS 1
#endif

#ifdef __ZTC__
#define CDECL cdecl
#define PAMDOS 1
#endif

#ifdef __WATCOMC__
#define CDECL 
#define SPLITPATH
#ifdef __OS2__
#define PAMOS2 1
#else
#define PAMDOS 1
#endif
#endif

#ifndef DOLLAR
#define dollar(p) (p)       /* dont bother to strip special dollars */
#endif

struct name
{
    struct name *           n_next;         /* Next in the list of names */
    char *                  n_orig;         /* Original Name */
    char *                  n_name;         /* Expanded Name */
    struct line *           n_line;         /* Dependencies */
    unsigned long           n_time;         /* Modify time of this name */
    uchar                   n_flag;         /* Info about the name */
};

#define N_MARK              0x01            /* For cycle check */
#define N_DONE              0x02            /* Name looked at */
#define N_TARG              0x04            /* Name is a target */
#define N_PREC              0x08            /* Target is precious */
#define N_DYND              0x10            /* Name was made by dyndep */
#define N_DOUBLE            0x20            /* Double colon */
#define N_MADE              0x40            /* Commands processed for this */
#define N_TSNAP             0x80            /* Time obtained from snapshot */

#define H_NONE              0
#define H_IF                1
#define H_IFN               2
#define H_ELSE              3
#define H_ENDIF             4
#define H_INCLUDE           5
#define H_ABORTEXIT         6
#define H_SET               7
#define H_STAT              8
#define H_IFE               9
#define H_IFNE              10
#define H_ABORTEXITNR       11
#define H_IFERR             12
 
struct  line
{
    struct line *           l_next;         /* Next line (for ::) */
    struct depend *         l_dep;          /* Dependants for this line */
    struct cmd *            l_cmd;          /* Commands for this line */
};

struct  depend
{
    struct depend *         d_next;         /* Next dependant */
    struct name *           d_name;         /* Name of dependant */
};

struct  cmd
{
    struct cmd *            c_next;         /* Next command line */
    struct lif *            c_lif;          /* LIF lines for this command */
    char *                  c_cmd;          /* Command line */
};

struct  lif
{
    struct lif *            f_next;         /* Next LIF line */
    char *                  f_lif;          /* LIF line */
};

struct  macro
{
    struct macro *          m_next;         /* Next variable */
    char *                  m_name;         /* Called ... */
    char *                  m_val;          /* Its value */
    char *                  m_sub;          /* Temp subst value */
    uchar                   m_flag;         /* Infinite loop check */
};

struct path 
{
    struct path *           p_next;         /* next path */
    char *                  p_ext;          /* the extension this maps */
    char *                  p_path;         /* the path to use */
};

#define M_LOOPCHK           0x01            /* For loop check */
#define M_ENV               0x02            /* ex-environment macro */
#define M_PERM              0x04            /* not resetable */

extern char *           myname;
extern struct name      namehead;
extern struct name *    firstname;
extern struct path *    pathhead;
extern struct macro *   macrohead;
extern bool             all;
extern bool             silent;
extern bool             confirm;
extern bool             ignore;
extern bool             rules;
extern bool             dotouch;
extern bool             quest;
extern bool             domake;
extern bool             display;
extern bool             tabreqd;
extern bool             swap;
extern bool             why;
extern char             str1[];
extern char             str2[];
extern int              lineno;
extern uchar            macrotype;
extern bool             ignoreshell;
extern unsigned int     errorlevel;

extern FILE *           ifile[4];
extern int              fln[4];
extern char             fname[4][80];
extern int              nestlvl;

#ifdef  PFPROTO                 /* use ANSI function prototypes */
#define PPA(args) args
#else
#define PPA(args) ()
#endif

extern struct macro *  addtomacro   PPA( (char *, char *) );
extern VOID            alltime      PPA( (char *) );
static VOID            check        PPA( (struct name *)  );
static VOID            checklif     PPA( (struct cmd *) );
extern VOID            circh        PPA( (VOID) );
static VOID            cleardynflag PPA( (struct name *) );
extern unsigned long   curtime      PPA( (VOID) );
static VOID            docmds1      PPA( (struct name *, struct line *) );
static VOID            docmds       PPA( (struct name *) );
extern VOID            dodisp       PPA( (char *, unsigned long) );
static VOID            doexp        PPA( (char * *, char *, int *, char *) );
extern struct cmd *    dofor        PPA( (struct name *, struct cmd * ) );
extern char *          dollar       PPA( (char *) );
static VOID            dosetcmd     PPA( (char *) );
static VOID            dostatcmd    PPA( (char *) );
static int             dosh         PPA( (char *, int) );
static int             dos_internal PPA( (char *) );
extern bool            dyndep       PPA( (struct name *) );
extern VOID CDECL      error        PPA( (char *,...) );
static VOID            expast       PPA( (struct name *) );
extern VOID            expand       PPA( (char *) );
extern struct macro *  extmacro     PPA( (char *, char *) );
extern VOID CDECL      fatal        PPA( (char *,...) );
extern VOID            ifeoc        PPA( (VOID) );
extern VOID            ifeof        PPA( (VOID) );
extern int             ifproc       PPA( (char *, int) );
extern VOID            input        PPA( (VOID) );
static int             isthere      PPA( (char *) );
static int             istrue       PPA( (char *) );
extern bool            getline      PPA( (char *) );
extern struct macro *  getmp        PPA( (char *) );
extern char *          gettok       PPA( (char * *) );
extern VOID            initml       PPA( (int, char *) );
static VOID            killlif      PPA( (VOID) );
static int             ktest        PPA( (char *, int) );
extern MAIN CDECL      main         PPA( (int, char * *, char * *) );
extern int             make         PPA( (struct name *, int) );
extern int             makeml       PPA( (int) );
static VOID            make1        PPA( (struct name *, struct line *, struct depend *) );
static VOID            makelif      PPA( (struct cmd *) );
extern VOID            makerules    PPA( (VOID) );
extern VOID            markmacros   PPA( (VOID) );
extern VOID            modtime      PPA( (struct name *) );
extern struct name *   newname      PPA( (char *) );
extern struct depend * newdep       PPA( (struct name *, struct depend *) );
extern struct cmd *    newcmd       PPA( (char *, struct cmd *, struct cmd * *) );
static struct lif *    newlif       PPA( (char *, struct lif *) );
extern VOID            newline      PPA( (struct name *, struct depend *, struct cmd *, int) );
extern VOID            precious     PPA( (VOID) );
extern VOID            prt          PPA( (VOID) );
extern char *          psa          PPA( (char *, char *) );
extern int             pspace       PPA( (int) );
extern char *          pstrstr      PPA( (char *, char *) );
extern VOID            resetcd      PPA( (void) );
extern VOID            searchtime   PPA( (struct name *) );
static VOID            setdmacros   PPA( (struct name *, struct depend *) );
extern struct macro *  setmacro     PPA( (char *, char *) );
extern VOID            snapshot     PPA( (VOID) );
extern char *          suffix       PPA( (char *) );
extern VOID            touch        PPA( (struct name *) );
static VOID            usage        PPA( (int) );

#ifdef STRDUP_NEEDED
extern char *          strdup       PPA( (char *) );
#endif
