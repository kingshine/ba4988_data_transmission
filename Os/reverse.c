
#include "inc\dictsys.h"
#include "inc\keytable.h"
#include "inc\stdlib.h"

extern U8 BK_SEL;
extern U8 BK_ADRL;
extern U8 BK_ADRH;

extern U8 hello[];
extern U8 head[];
extern U8 str_SysReadCom[];
extern U8 str_DataBankSwitch[];
extern U8 str_SysWriteCom[];
extern U8 str_continue[];
/*
*0x03E4 is 0x0E
*0x03E5 is 0x80
*0x202A is 0x02
*0x202F is 0x2F
start 0x1000
DataBankSwitch(9, 4, 0x0001);
start 0x9000
DataBankSwitch(9, 4, 0x0E80);
start 0xD000
DataBankSwitch(9, 3, 0x0E88);
start 0x
DataBankSwitch(9, 4, (0x07<<2)+0x0E80);
*/

FAR U8 Reverse()
{
	MsgType msg;
    U8 buffer[8];
    U8 size = 4;
    GuiInit(); /*Gui OS 初始化，使用前一定要调用 */
    SysMemInit(MEM_HEAP_START,MEM_HEAP_SIZE);	/*初始化堆*/
    SysLCDClear();
    SysPrintString(11, 23, hello);

    SysOpenCom(UART_MODE, BAUD_115200_BPS, DISABLE_PARITY);

    while (1)
    {
		if(!GuiGetMsg(&msg))		continue;
		if(!GuiTranslateMsg(&msg))	continue;

		if (msg.type == WM_COM)
        {
            SysPrintString(11, 23, str_SysReadCom);
            if (SysReadCom(buffer, &size))
            {
                continue;
            }
            if (size != 4)
            {
                break;
            }
            if (buffer[0] == 0 || buffer[0] >4)
            {
                break;
            }
            
            SysPrintString(11, 23, str_DataBankSwitch);
            DataBankSwitch(9, buffer[0], (((U16)buffer[2])<<8)+buffer[1]);

            SysPrintString(11, 23, str_SysWriteCom);
            U8* p = (U8*)0x9000;
            for (U8 i=0; i<buffer[0]; i++)
            {
                for (U8 j=0; j<0x20; j++)
                {
                    SysWriteCom(0x80, p);
                    p += 0x80;
                }
            }
        }
    }

    SysCloseCom();
    return 0;
}

FAR U8 Back()
{
	.asm
	nop
	.endasm
	U16	Data_Bank_Bak;
    MsgType Msg;
    U16 i = 0;
    GuiInit(); /*Gui OS 初始化，使用前一定要调用 */
    SysMemInit(MEM_HEAP_START,MEM_HEAP_SIZE);	/*初始化堆*/
    SysLCDClear();
    SysPrintString(11, 23, hello);

	GetDataBankNumber(9,&Data_Bank_Bak);
	DataBankSwitch(9,4,Data_Bank_Bak);
    SysWriteCom(0x04, head);

    GuiGetMsg(&Msg);
    SysCloseCom();
    return 0;
}