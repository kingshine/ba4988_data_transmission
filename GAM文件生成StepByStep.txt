1、将在OS目录中生成程序的obj文件copy到dwn目录下。
(如：psnake.obj、psnakedata.obj、psnakefar.obj)
2、将在OS目录中相应obj文件的lst文件opy到dwn目录下。
(如：psnake.lst、psnakedata.lst、psnakefar.lst)
3、执行makepsn.bat，即可得到一个"贪食蛇.gam"的文件。

（以上是以贪食蛇为例，介绍的。要自己DIY生成另外一个
游戏的.gam文件，还有些比较麻烦的地方。
有三个方法解决：
1、你自己根据贪食蛇的例子摸索。
2、我给你讲解清楚到底要怎么做。（要等有时间了才可以。）
3、我们提供一个工具。（要等有时间了才可以。））